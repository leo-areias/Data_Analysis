Gists List

**A simple Gist of a codecademy exercise in scatter plots and correlation**
 . https://gist.github.com/leo-areias/fd12e905c347d23d1fafc7f6271bbed8

**A simple Gist,  using for iteration to create graphs to each column of the table**
 . https://gist.github.com/leo-areias/8c854ff16587fb1aa4f608a471191d59

**Finding the best deal for your client and helping them to understand how airline prices change based on different factors.**
 . https://gist.github.com/codecademydev/53472bf77b995508ac78fd3de0ee9ac9
